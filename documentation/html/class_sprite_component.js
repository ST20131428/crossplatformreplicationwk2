var class_sprite_component =
[
    [ "SpriteComponent", "class_sprite_component.html#a9a0e6fb9ca03670f9182cde1b50a2e11", null ],
    [ "~SpriteComponent", "class_sprite_component.html#add14acc8523a724c112e6c93b750b60e", null ],
    [ "Draw", "class_sprite_component.html#a8777f64b152543df4c05f195895bb546", null ],
    [ "GetOrigin", "class_sprite_component.html#a2d75c9f26b8b6a5da582bf668f6d2118", null ],
    [ "SetOrigin", "class_sprite_component.html#a086e0f97a9a516e9ea632878d6362d19", null ],
    [ "SetTexture", "class_sprite_component.html#a3f4da5112d7d7b772659d403394a8cb7", null ]
];