var class_network_manager_client =
[
    [ "GetAvgRoundTripTime", "class_network_manager_client.html#ae746983101c7ac35571ffcda75e76600", null ],
    [ "GetLastMoveProcessedByServerTimestamp", "class_network_manager_client.html#a28c8bb630ad5c78f295b0571874b83a6", null ],
    [ "GetPlayerId", "class_network_manager_client.html#a937a4c804cadbf3e23e18ed50266ca2a", null ],
    [ "GetRoundTripTime", "class_network_manager_client.html#aa575448841a851857f8d1d2573b62dfc", null ],
    [ "ProcessPacket", "class_network_manager_client.html#af2bb017e7e33b1c85ef1313b3e2cbb8d", null ],
    [ "SendOutgoingPackets", "class_network_manager_client.html#a87ab7e10c2e78675ffc248a0ab3bcf68", null ]
];