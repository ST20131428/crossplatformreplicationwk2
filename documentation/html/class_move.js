var class_move =
[
    [ "Move", "class_move.html#a4b1acc3a67d30c385ad9a6000526393a", null ],
    [ "Move", "class_move.html#a56b6d679de3facb7f979a835cb9b40a9", null ],
    [ "GetDeltaTime", "class_move.html#a4363c7f03b82c4f7741d1a5b00aa1874", null ],
    [ "GetInputState", "class_move.html#a984208555e8ca47388d6af7a5b20cc80", null ],
    [ "GetTimestamp", "class_move.html#a46f95389d6ce3b639085ce14b8725734", null ],
    [ "Read", "class_move.html#a303071176f2ed00328636c36dcc05fec", null ],
    [ "SetDeltaTime", "class_move.html#ae63933fd92b06b7f837a3475bce4abed", null ],
    [ "setInputState", "class_move.html#a0ddd8f22c7538f68ae0c23e33cc107f0", null ],
    [ "SetTimestamp", "class_move.html#a23986be815f7ece501987adf79aa265f", null ],
    [ "Write", "class_move.html#a975919a03d8638a6a5383641e9192405", null ]
];