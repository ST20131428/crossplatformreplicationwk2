var class_player_server =
[
    [ "PlayerServer", "class_player_server.html#ab9883626b285a3c390ac5a4726c37cf7", null ],
    [ "HandleDying", "class_player_server.html#a1335248b0a94b4c69c3a0261bdfd6cbb", null ],
    [ "SetPlayerControlType", "class_player_server.html#a338b3df9fcf19399bd626d60d7694e0d", null ],
    [ "TakeDamage", "class_player_server.html#a6fc2673f725fe576b1194deef5dbdbcd", null ],
    [ "Update", "class_player_server.html#aa6fec32773759d13dd80140e3cb28859", null ]
];