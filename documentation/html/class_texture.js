var class_texture =
[
    [ "Texture", "class_texture.html#a378e010f447a648cfae31616863ac750", null ],
    [ "~Texture", "class_texture.html#a09c4bcb7462f64c1d20fa69dba3cee8a", null ],
    [ "GetData", "class_texture.html#aa10aa2cc059ba3e3a0f43ec5d922ac65", null ],
    [ "GetHeight", "class_texture.html#a60949a1d771044b6fc1ffb5913a94f09", null ],
    [ "GetWidth", "class_texture.html#aa63fcfeb4f4064a4d1ccec1386fcd315", null ]
];