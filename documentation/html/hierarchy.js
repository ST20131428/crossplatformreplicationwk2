var hierarchy =
[
    [ "ByteSwapper< T, tSize >", "class_byte_swapper.html", null ],
    [ "ByteSwapper< T, 1 >", "class_byte_swapper_3_01_t_00_011_01_4.html", null ],
    [ "ByteSwapper< T, 2 >", "class_byte_swapper_3_01_t_00_012_01_4.html", null ],
    [ "ByteSwapper< T, 4 >", "class_byte_swapper_3_01_t_00_014_01_4.html", null ],
    [ "ByteSwapper< T, 8 >", "class_byte_swapper_3_01_t_00_018_01_4.html", null ],
    [ "ClientProxy", "class_client_proxy.html", null ],
    [ "Engine", "class_engine.html", [
      [ "Client", "class_client.html", null ],
      [ "Server", "class_server.html", null ]
    ] ],
    [ "exception", null, [
      [ "UnknownGameObjectType", "class_unknown_game_object_type.html", null ]
    ] ],
    [ "GameObject", "class_game_object.html", [
      [ "Bullet", "class_bullet.html", [
        [ "BulletClient", "class_bullet_client.html", null ],
        [ "BulletServer", "class_bullet_server.html", null ]
      ] ],
      [ "Player", "class_player.html", [
        [ "PlayerClient", "class_player_client.html", null ],
        [ "PlayerServer", "class_player_server.html", null ]
      ] ]
    ] ],
    [ "GameObjectRegistry", "class_game_object_registry.html", null ],
    [ "GetRequiredBits< tValue >", "struct_get_required_bits.html", null ],
    [ "GetRequiredBitsHelper< tValue, tBits >", "struct_get_required_bits_helper.html", null ],
    [ "GetRequiredBitsHelper< 0, tBits >", "struct_get_required_bits_helper_3_010_00_01t_bits_01_4.html", null ],
    [ "GraphicsDriver", "class_graphics_driver.html", null ],
    [ "std::hash< SocketAddress >", "structstd_1_1hash_3_01_socket_address_01_4.html", null ],
    [ "InputManager", "class_input_manager.html", null ],
    [ "InputMemoryBitStream", "class_input_memory_bit_stream.html", null ],
    [ "InputMemoryStream", "class_input_memory_stream.html", null ],
    [ "InputState", "class_input_state.html", null ],
    [ "LinkingContext", "class_linking_context.html", null ],
    [ "Move", "class_move.html", null ],
    [ "MoveList", "class_move_list.html", null ],
    [ "NetworkManager", "class_network_manager.html", [
      [ "NetworkManagerClient", "class_network_manager_client.html", null ],
      [ "NetworkManagerServer", "class_network_manager_server.html", null ]
    ] ],
    [ "OutputMemoryBitStream", "class_output_memory_bit_stream.html", null ],
    [ "OutputMemoryStream", "class_output_memory_stream.html", null ],
    [ "Quaternion", "class_quaternion.html", null ],
    [ "RandGen", "class_rand_gen.html", null ],
    [ "ReceivedPacket", "class_received_packet.html", null ],
    [ "RenderManager", "class_render_manager.html", null ],
    [ "ReplicationCommand", "struct_replication_command.html", null ],
    [ "ReplicationManagerClient", "class_replication_manager_client.html", null ],
    [ "ReplicationManagerServer", "class_replication_manager_server.html", null ],
    [ "SocketAddress", "class_socket_address.html", null ],
    [ "SocketAddressFactory", "class_socket_address_factory.html", null ],
    [ "SocketUtil", "class_socket_util.html", null ],
    [ "SpriteComponent", "class_sprite_component.html", null ],
    [ "TCPSocket", "class_t_c_p_socket.html", null ],
    [ "Test", null, [
      [ "ByteSwapTestHarness", "class_byte_swap_test_harness.html", null ],
      [ "GameObjectRegistryTestHarness", "class_game_object_registry_test_harness.html", null ],
      [ "GameObjectTestHarness", "class_game_object_test_harness.html", null ],
      [ "InputStateTestHarness", "class_input_state_test_harness.html", null ],
      [ "MathTestHarness", "class_math_test_harness.html", null ],
      [ "MemoryBitStreamTestHarenss", "class_memory_bit_stream_test_harenss.html", null ],
      [ "MemoryStreamTestHarness", "class_memory_stream_test_harness.html", null ],
      [ "MoveTestHarness", "class_move_test_harness.html", null ],
      [ "PlayerTestHarness", "class_player_test_harness.html", null ],
      [ "RandomGenTestHarness", "class_random_gen_test_harness.html", null ],
      [ "SocketAddressFactoryTestHarness", "class_socket_address_factory_test_harness.html", null ],
      [ "SocketAddressTestHarenss", "class_socket_address_test_harenss.html", null ],
      [ "SocketUtilTestHarness", "class_socket_util_test_harness.html", null ],
      [ "TCPSocketTestHarness", "class_t_c_p_socket_test_harness.html", null ],
      [ "UDPSocketTestHarness", "class_u_d_p_socket_test_harness.html", null ],
      [ "Vector3TestHarness", "class_vector3_test_harness.html", null ],
      [ "WorldTestHarness", "class_world_test_harness.html", null ]
    ] ],
    [ "TestObject", "class_test_object.html", null ],
    [ "Texture", "class_texture.html", null ],
    [ "TextureManager", "class_texture_manager.html", null ],
    [ "Timing", "class_timing.html", null ],
    [ "TypeAliaser< tFrom, tTo >", "class_type_aliaser.html", null ],
    [ "UDPSocket", "class_u_d_p_socket.html", null ],
    [ "Vector3", "class_vector3.html", null ],
    [ "WeightedTimedMovingAverage", "class_weighted_timed_moving_average.html", null ],
    [ "WindowManager", "class_window_manager.html", null ],
    [ "World", "class_world.html", null ]
];