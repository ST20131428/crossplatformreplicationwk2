var class_render_manager =
[
    [ "AddComponent", "class_render_manager.html#a04d570fbfc55b57fa18f69df8f54e872", null ],
    [ "GetComponentIndex", "class_render_manager.html#a71dcd995f1557a166dc59b83c18a69fd", null ],
    [ "RemoveComponent", "class_render_manager.html#a7448a0d2f4547e9aa5fa35e524a846f2", null ],
    [ "Render", "class_render_manager.html#a1e0788708fda748a7ff5e2730ed478f1", null ],
    [ "RenderComponents", "class_render_manager.html#a66d99aba07818e73a4781af417baef92", null ]
];