var class_bullet =
[
    [ "EYarnReplicationState", "class_bullet.html#a78de4fd39a2be744466e8400258c5aef", [
      [ "EYRS_Pose", "class_bullet.html#a78de4fd39a2be744466e8400258c5aefafd28547862336fcadc9f02fd9fb19518", null ],
      [ "EYRS_Color", "class_bullet.html#a78de4fd39a2be744466e8400258c5aefa46dcdc121077339e905be701d4e8e31f", null ],
      [ "EYRS_PlayerId", "class_bullet.html#a78de4fd39a2be744466e8400258c5aefa6bf72cb2827fd1e1c1535588dec7e99e", null ],
      [ "EYRS_AllState", "class_bullet.html#a78de4fd39a2be744466e8400258c5aefac09be2e1d59a6c1dac95dd1ab437e9c7", null ]
    ] ],
    [ "Bullet", "class_bullet.html#acd7befc0bc18907cc1d871d37bbdddeb", null ],
    [ "GetAllStateMask", "class_bullet.html#ae8417cbb816e604781524017309c8e70", null ],
    [ "GetPlayerId", "class_bullet.html#a0bd595ce6e85fdc1f1d5bf9be1b0f34b", null ],
    [ "GetVelocity", "class_bullet.html#af6425874f0c9866019fec97151029a7b", null ],
    [ "HandleCollisionWithPlayer", "class_bullet.html#a7adbea1f621df7dbed177ea4fca6ccb8", null ],
    [ "InitFromShooter", "class_bullet.html#ac451fe6c15659aebc7b23fb3697cd99e", null ],
    [ "SetPlayerId", "class_bullet.html#ac05fce158053c1bb8bf4a27b0b8c1b6b", null ],
    [ "SetVelocity", "class_bullet.html#a7e76c9fdc512e447bfe408487eb80695", null ],
    [ "Update", "class_bullet.html#acc341563f05697212c1d5b7db1696125", null ],
    [ "Write", "class_bullet.html#a8cee3337c767d003cd1dee882ad99c39", null ],
    [ "mMuzzleSpeed", "class_bullet.html#a20bad411eb8398839ef8483dce0ada3e", null ],
    [ "mPlayerId", "class_bullet.html#afa8afd5e5229aa2914226546a307e7ad", null ],
    [ "mVelocity", "class_bullet.html#ae12cce8571f7d3254f9d664db5482790", null ]
];