var class_graphics_driver =
[
    [ "~GraphicsDriver", "class_graphics_driver.html#a6b0ea36b7200cec6ec979f840de44ec2", null ],
    [ "Clear", "class_graphics_driver.html#ae7309402adff3c5f0ea28e4edf5cd908", null ],
    [ "GetLogicalViewport", "class_graphics_driver.html#aaf67e724844b3dda2310c8128be082be", null ],
    [ "GetRenderer", "class_graphics_driver.html#a6523348f10947814f1455a3c47f360a4", null ],
    [ "Present", "class_graphics_driver.html#a3567f3a64c9be2f6fa54c863ad9f45ac", null ]
];