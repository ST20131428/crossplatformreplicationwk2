var class_timing =
[
    [ "Timing", "class_timing.html#a6610a038a1c4b8c009c275d47efca90c", null ],
    [ "GetDeltaTime", "class_timing.html#a6eef615d80a9dddd9852622def6375a8", null ],
    [ "GetFrameStartTime", "class_timing.html#a561a73b63fd7df3270d26df66c2ecfe9", null ],
    [ "GetTime", "class_timing.html#a72f61c346a2cd5e279a7fecf19f89725", null ],
    [ "GetTimef", "class_timing.html#a4eced8ea0ef50964981794b892f8bcbd", null ],
    [ "Update", "class_timing.html#ac0f5b331ef3379abe01efe4ffe4b86b0", null ]
];