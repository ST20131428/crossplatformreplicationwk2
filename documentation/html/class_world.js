var class_world =
[
    [ "AddGameObject", "class_world.html#adca70934bdbbb6b0605e52fc7e3a367f", null ],
    [ "GetGameObjectCount", "class_world.html#ace44c962b7779eada675bfb3e815d359", null ],
    [ "GetGameObjects", "class_world.html#a48c44a0400e2f0e879c0a4880e29cb5f", null ],
    [ "RemoveGameObject", "class_world.html#a79ed86c4b6cfc4823216aaf37dd8634b", null ],
    [ "Update", "class_world.html#aec4c79eb3becec1110cc910bf1555181", null ]
];